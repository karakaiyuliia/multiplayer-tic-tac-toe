﻿/*
file:		MainWindow.xams.cs
purpose:	GUI logic
author:		Yuliia Karakai and Brandon Antunes
date:		04/08/2019
*/

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using GameLibrary;


namespace MultiplayerGame
{

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 
    [CallbackBehavior(ConcurrencyMode = ConcurrencyMode.Reentrant, UseSynchronizationContext = false)]
    public partial class MainWindow : Window, ICallback
    {
        // Member variables
        private IGameService serv = null;
        private bool callbacksEnabled = false;
        private GameService service = new GameService();
        //GameService service = new GameService();

        public MainWindow()
        {
            InitializeComponent();

            try
            {
                // Connect to the WCF service endpoint called "ShoeService" 
                DuplexChannelFactory<IGameService> channel = new DuplexChannelFactory<IGameService>(this, "GameEndpoint");
                serv = channel.CreateChannel();

                // Subscribe to the callbacks
                callbacksEnabled = serv.ToggleCallbacks();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        // Implement ICallback contract
        private delegate void ClientUpdateDelegate(CallbackInfo info);

        public void UpdateGui(CallbackInfo info)
        {
            string winner;
            if (System.Threading.Thread.CurrentThread == this.Dispatcher.Thread)
            {
                foreach (UIElement el in btnGrid.Children)
                {
                    if (el.GetType() == typeof(Button))
                    {
                        Button btn = (Button)el;

                        if (info.Xloc.Count > 0)
                        {
                            if (btn.Name == "btn" + info.Xloc[0])
                            {
                                btn.Content = "X";
                                btn.Foreground = Brushes.Red;
                                winner = service.IsWin(Int32.Parse(btn.Name.Remove(0, 3)), info.XlocWin, "X");

                                if (winner != null)
                                {
                                    MessageBox.Show(winner + " won!");
                                    ClearButtons();
                                }
                            }

                        }
                         if (info.Yloc.Count > 0)
                        {
                            if (btn.Name == "btn" + info.Yloc[0])
                            {
                                btn.Content = "Y";
                                btn.Foreground = Brushes.Blue;
                                winner = service.IsWin(Int32.Parse(btn.Name.Remove(0, 3)), info.YlocWin, "Y");

                                if (winner != null)
                                {
                                    MessageBox.Show(winner + " won!");
                                    ClearButtons();
                                }
                            }
                        }
                         if (info.Oloc.Count > 0)
                        {
                            if (btn.Name == "btn" + info.Oloc[0])
                            {
                                btn.Content = "O";
                                btn.Foreground = Brushes.Green;
                                winner = service.IsWin(Int32.Parse(btn.Name.Remove(0, 3)), info.OlocWin, "O");

                                if (winner != null)
                                {
                                    MessageBox.Show(winner + " won!");
                                    ClearButtons();
                                }
                            }
                        }
                    }
                }

            }
            else
            {
                // Only the main (dispatcher) thread can change the GUI
                this.Dispatcher.BeginInvoke(new ClientUpdateDelegate(UpdateGui), info);
            }
        }


        private void OnClick(object sender, RoutedEventArgs e)
        {
            // cast the sender to a Button
            Button button = sender as Button;

            if (button.Content == null)
            {
                try
                {
                    List<Move> values = serv.MakeMove(button.Name);
                    button.Content = values[0].value;
                    Color color = (Color)ColorConverter.ConvertFromString(values[0].color);
                    button.Foreground = new SolidColorBrush(color);
                    
                    
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

            }
        }

        private void ClearButtons()
        {
            //Clear Button Display
            btn10.Content = null;
            btn11.Content = null;
            btn12.Content = null;
            btn13.Content = null;
            btn14.Content = null;

            btn20.Content = null;
            btn21.Content = null;
            btn22.Content = null;
            btn23.Content = null;
            btn24.Content = null;

            btn30.Content = null;
            btn31.Content = null;
            btn32.Content = null;
            btn33.Content = null;
            btn34.Content = null;

            btn40.Content = null;
            btn41.Content = null;
            btn42.Content = null;
            btn43.Content = null;
            btn44.Content = null;

            //Clear Button List
            service.ClearLists();
        }

    }
}
