﻿/*
file:		Callback.cs
purpose:	callbacks
author:		Yuliia Karakai and Brandon Antunes
date:		04/08/2019
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.Windows.Media;

namespace GameLibrary
{
    [DataContract]
    public class CallbackInfo
    {
        [DataMember] public List<int> Xloc { get; private set; }
        [DataMember] public List<int> Yloc { get; private set; }
        [DataMember] public List<int> Oloc { get; private set; }
        [DataMember] public List<int> XlocWin { get; private set; }
        [DataMember] public List<int> YlocWin { get; private set; }
        [DataMember] public List<int> OlocWin { get; private set; }

        public CallbackInfo()
        {

        }

        public CallbackInfo(List<int> c, List<int> d, List<int> e, List<int> f, List<int> h, List<int> g)
        {
            Xloc = c;
            Yloc = d;
            Oloc = e;
            XlocWin = f;
            YlocWin = h;
            OlocWin = g;
        }
    }
}
