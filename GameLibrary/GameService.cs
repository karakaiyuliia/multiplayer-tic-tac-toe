﻿/*
file:		GameService.cs
purpose:	game logic
author:		Yuliia Karakai and Brandon Antunes
date:		04/08/2019
*/

using System;
using System.Collections.Generic;
using System.ServiceModel;

namespace GameLibrary
{

    // Define a callback contract for the client to implement
    public interface ICallback
    {
        [OperationContract(IsOneWay = true)] void UpdateGui(CallbackInfo info);
    }

    // Define a WCF service contract for the "service"
    [ServiceContract(CallbackContract = typeof(ICallback))]

    public interface IGameService
    {
        [OperationContract] bool ToggleCallbacks();
        [OperationContract] List<Move> MakeMove(string btnName);
    }

    public class Move
    {
        public string color;
        public string value;
    }

    // Define an implementation for the service contract
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]

    public class GameService : IGameService
    {
        private static List<int> Xloc = new List<int>();
        private static List<int> XlocWin = new List<int>();
        private static List<int> Yloc = new List<int>();
        private static List<int> YlocWin = new List<int>();
        private static List<int> Oloc = new List<int>();
        private static List<int> OlocWin = new List<int>();
        private static HashSet<ICallback> callbacks = null;

        private bool isX = true;
        private bool isY = false;
        private bool is0 = false;

        public GameService()
        {
            callbacks = new HashSet<ICallback>();
        }

        public bool ToggleCallbacks()
        {
            ICallback cb = OperationContext.Current.GetCallbackChannel<ICallback>();

            if (callbacks.Contains(cb))
            {
                callbacks.Remove(cb);
                return false;
            }
            else
            {
                callbacks.Add(cb);
                return true;
            }
        }       


        public List<Move> MakeMove(string btnName)
        {
            Xloc.Clear();
            Yloc.Clear();
            Oloc.Clear();
            Move move = new Move();
            List<Move> value = new List<Move>();

            if (isX)
            {
                move.color = "#FFFF0000";
                move.value = "X";
                value.Add(move);
                isX = false;
                isY = true;
                Xloc.Add(Int32.Parse(btnName.Remove(0, 3)));
                Xloc.Sort();

                XlocWin.Add(Int32.Parse(btnName.Remove(0, 3)));
                XlocWin.Sort();
            }
            else if (isY)
            {
                move.color = "#FF0000FF";
                move.value = "Y";
                value.Add(move);
                isY = false;
                is0 = true;
                Yloc.Add(Int32.Parse(btnName.Remove(0, 3)));
                if (Yloc.Count == 7)
                {
                    //MessageBox.Show("Game is over Cats Game!");
                    //GameisOver("Cats Game!, Nobody");
                }
                Yloc.Sort();
                YlocWin.Add(Int32.Parse(btnName.Remove(0, 3)));
                YlocWin.Sort();
            }
            else if (is0)
            {
                move.color = "#FF008000";
                move.value = "O";
                value.Add(move);
                is0 = false;
                isX = true;
                Oloc.Add(Int32.Parse(btnName.Remove(0, 3)));
                Oloc.Sort();

                OlocWin.Add(Int32.Parse(btnName.Remove(0, 3)));
                OlocWin.Sort();
            }
            updateAllClients();

            return value;
        }
        

        private void updateAllClients()
        {
            CallbackInfo info = new CallbackInfo(Xloc, Yloc, Oloc, XlocWin, YlocWin, OlocWin);

            foreach (ICallback cb in callbacks)
                cb.UpdateGui(info);
        }

        public void ClearLists()
        {

            Xloc.Clear();
            Yloc.Clear();
            Oloc.Clear();
            XlocWin.Clear();
            YlocWin.Clear();
            OlocWin.Clear();

        }

        //Check for win Case
        public string IsWin(int lastPLaced, List<int> list, string player)
        {
            //test vertical
            if ((list.Contains(lastPLaced) && list.Contains(lastPLaced + 1) && list.Contains(lastPLaced + 2))
            || (list.Contains(lastPLaced) && list.Contains(lastPLaced - 1) && list.Contains(lastPLaced - 2))
            || (list.Contains(lastPLaced) && list.Contains(lastPLaced - 1) && list.Contains(lastPLaced + 1))
            )
            {
                isX = true;
                return player;
            }
            //test horizontal
            else if ((list.Contains(lastPLaced) && list.Contains(lastPLaced + 10) && list.Contains(lastPLaced + 20))
            || (list.Contains(lastPLaced) && list.Contains(lastPLaced - 10) && list.Contains(lastPLaced - 20))
            || (list.Contains(lastPLaced) && list.Contains(lastPLaced - 10) && list.Contains(lastPLaced + 10))
            )
            {
                isX = true;
                return player;
            }
            //test angle
            else if ((list.Contains(lastPLaced) && list.Contains(lastPLaced + 11) && list.Contains(lastPLaced + 22))
            || (list.Contains(lastPLaced) && list.Contains(lastPLaced - 11) && list.Contains(lastPLaced - 22))
            || (list.Contains(lastPLaced) && list.Contains(lastPLaced - 11) && list.Contains(lastPLaced + 11))
            || (list.Contains(lastPLaced) && list.Contains(lastPLaced - 9) && list.Contains(lastPLaced - 18))
            || (list.Contains(lastPLaced) && list.Contains(lastPLaced + 9) && list.Contains(lastPLaced + 18))
            || (list.Contains(lastPLaced) && list.Contains(lastPLaced + 9) && list.Contains(lastPLaced - 9))
            )
            {
                isX = true;
                return player;
            }

            else
            {
                return null;
            }

        }

    }
}
