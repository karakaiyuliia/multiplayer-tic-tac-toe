﻿/*
file:		Program.cs
purpose:	server
author:		Yuliia Karakai and Brandon Antunes
date:		04/08/2019
*/

using System;
using System.ServiceModel;
using GameLibrary;

namespace GameServiceHost
{
    public class Program
    {
        static void Main(string[] args)
        {
            ServiceHost servHost = null;

            try
            {
                // Register the service Address
                servHost = new ServiceHost(typeof(GameService));

                // Register the service Contract and Binding
                // servHost.AddServiceEndpoint(typeof(IShoe), new NetTcpBinding(), "ShoeService");

                // Run the service
                servHost.Open();
                Console.WriteLine("Service started. Press any key to quit.");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                // Wait for a keystroke
                Console.ReadKey();
                if (servHost != null)
                    servHost.Close();
            }
        }
    }
}
